import React from 'react';
import { Switch,Route,Redirect} from 'react-router-dom';
import Header from '../src/component/main-Header/Header';
import Home from '../src/component/Home-title/Home';
import Features from "../src/component/fan/Features";
import Brands from "../src/component/API/Brands";
import Work from '../src/component/Works/Work';
import Support from '../src/component/Support-sec/Support';
import Singup from '../src/component/Sing/Singup';
import ScrollToTop from "react-scroll-to-top";


 
const App = () => {
return (
  <>

  <ScrollToTop smooth />
 
 <Header 
  navbar1="Home"
  navbar2="Features"
  navbar3="How It Works"
  navbar4="Support"
  navbar5=" Sing in"
 />
 
  
<Switch>

<Route exact path="/" component={ () =>
  <Home
  title="The new standard in" 
  title1=" online payments"
  para="It should be noted that although application software is"
  para1="thought of as a program, it can be anything that runs on a"
  para2="computer."
  link="All popular payment methods"
  link1="Payments around the world"
  link2="n-depth data insights"
  btn=" Get Started!"

  />
} />

<Route exact path="/features" component={Features}/>
<Route exact path="/work" component={Work}/>
<Route exact path="/support" component={Support} />
<Route exact path="/brands" component={Brands} />
<Redirect to="/" />
 
</Switch>

  </>
)
}

export default App;


