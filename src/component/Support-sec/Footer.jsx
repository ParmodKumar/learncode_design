import React from 'react';

import { NavLink } from 'react-router-dom';
const Footer = (props) => {
return(
    
        <>
     
               
               <div className="footer-sec">
                 
                   <div className={props.class}>
    <div className="footer-img" >
<img src={props.img} alt="footer" />
    </div>
    <div className="contact-info">
                <div className="nav-sec">
                
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" >{props.contact1}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" >{props.contact2}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" >{props.contact3}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" >{props.contact4}</NavLink>
                  </li>
                 
                 
          </div>
          </div>
   
    </div>
</div>



        </>
)
}

export default Footer;