import React from 'react';
import { NavLink } from 'react-router-dom';

const Quick = (props) => {
return(
    <>
    <div className="single-footer-widget">

    <h6 className="bottom">{props.title}
    </h6>
    
                <div className="list">
                
                  <li className="nav-item">
                    <NavLink className="nav-link listnav" to="/" >{props.list1}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link listnav" to="/" >{props.list2}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link listnav" to="/" >{props.list3}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link listnav" to="/" >{props.list4}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link listnav" to="/" >{props.list5}</NavLink>
                  </li>
                 
          </div>

        
          </div>
    </>
)
}

export default Quick;