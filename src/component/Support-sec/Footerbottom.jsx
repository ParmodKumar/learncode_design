import React from 'react';
import { NavLink } from 'react-router-dom';

const Footerbottom = (props) => {
return(
    <>
  
<div className="container">
<div className="row" id="support">
  <div className="col-sm-6">
<div className="contact-content-box">
<h3>{props.title}</h3>
<p>{props.title1}</p>

</div>
  </div>
  <div className="col-sm-6">
  <div className="contact-connect">
                   
<NavLink className="btn btn-outline sing my-2 my-sm-0" to="/" >{props.btn}</NavLink>
                   <NavLink className="btn btn-outline Talk my-2 my-sm-0" to="/" >{props.btn1}</NavLink>
                              
                                </div>
  </div>
</div>
 </div>
  
    </>
)
}

export default Footerbottom;