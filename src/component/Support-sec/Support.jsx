import React from 'react';
import '../Support-sec/support.css';
import Footerbottom from '../Support-sec/Footerbottom';
import Footer from '../Support-sec/Footer';
import '../Support-sec/footer.css';
import footer from '../Support-sec/imges/footer.png';
import Quick from '../Support-sec/Quick';


const Support = () => {

return(
      <>
        <div className="container-fluid payment-processing-contact">
          <div className="container">
            <div className="row">
              <Footerbottom 
                title="Ready to get Started?"
                title1="No setup costs or contract - start taking payments today"
                btn="Sign Up Now"
                btn1="Talk to Sales"
              />
            </div>
          </div>
        </div>
          <div className="container-fluid footer-area-four" id="support">  
                <div className="container">
                  <div className="row mb-5 pb-5">
                    <div className="col-sm-4">
                    <Footer 
                    img={footer}
                    contact1="1828 Parmod kumar Raipura, Pb 152116"
                    contact2="(224) 228-8475"
                    contact3="support@pluck.com"
                    contact4="www.pluck.com"
                    />
                    </div>
                    <div className="col-sm-2">
                    
                    <Quick 
                    title="Quick Links"
                    list1="Home"
                    list2="Features"
                    list3="Screenshots"
                    list4="Pricing"
                    list5="News"
                    />
                   
                    </div>
                    <div className="col-sm-3 Support">
                    <Quick 
                    title="Support"
                    list1="Quick Support"
                    list2="Privacy Policy"
                    list3="Copyright"
                    list4="Terms & Conditions"
                    list5="Testimonials"
                    />
                    </div>
                    <div className="col-sm-3">
                    <Quick 
                    title="My Account"
                    list1="Managed Account"
                    list2="Create Account"
                    list3="Download Software"
                    list4="Support Center"
                    list5="Account Security"
                    />
                    </div>
                  </div>
                </div>
              
          </div>
       
</>
);
}

export default Support;



