import React from 'react';
import Button from '@material-ui/core/Button'
import Link from '@material-ui/core/Link';
import { NavLink } from 'react-router-dom';
import SwapHorizIcon from '@material-ui/icons/SwapHoriz';

const Form = () => {
    const apiUrl = 'http://localhost/dev/tcxapp/reactapi/products';


    return(
        <>
        
        <div className="login-form">
        <h3 className="text-center">Welcome Back!</h3>
            <div className="group-from">
        <form>
        <label>Email</label>
        <input type="email" class="form-control" placeholder="Email Address"/>
        <label>Password</label>
        <input type="password" class="form-control" placeholder="Password"  />
        <div className="btnlogin">
        <Button variant="contained" color="secondary" className="longbtn">
  Login
</Button>
<div className="line-sec">
<Link to="/" className="Create-sec">
Create a new account
      </Link>
      <Link to="/" className="Create-sec float-right">
      Forgot your password?
      </Link>
      <div className="goback">
      <NavLink to="/"><SwapHorizIcon className="goback-sec"/></NavLink>
      </div>
</div>

</div>
        </form>
       
        </div>
        </div>
      
     
       
        </>

    )
}

export default Form;