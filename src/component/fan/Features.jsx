import React from 'react';
import Card from '../fan/Card';
import '../fan/features.css';
import Icondata from '../fan/Icondata';
import Srevice from '../fan/Services';
import Btn from '../main-Header/Btn';
import Dashboard from '../fan/Dashboard';
import photo from '../fan/Imges/photo.jpg';
import Smart from '../fan/Smart';
import photo1 from '../fan/Imges/photo1.png';


const Features = () => {
    return(
<>
    <div className="container-fluid background-sec">
        <div className="container icon-sec" id="features">
            <Srevice 
            title1="Services"
            title2="Our Features"
            class="services"
            title3="Quis ipsum suspendisse ultrices gravida. Risus commodo viverra"
            title4="maecenas accumsan lacus vel facilisis."
          
           
            />
                <div className="row">
                    <div className="col-sm-12 mx-auto">
                        <div className="row gy-4">
                            
                            {
                            Icondata.map ((val,id)=>{
                                return <Card 
                                key={id}
                                title={val.title}
                                
                                />
                            })  
                            }
                        </div>
                    </div>
                </div>
        </div>
    </div>
        <div className="container-fluid">
            <div className="container">
                <Srevice 
                        title1="SELL SMARTER"
                        title2="Accept Payments Online. Get Paid"
                        title="Faster."
                        class="services"
                        title3="Keep track of all your payments using our beautifully designed payments"
                        title4="dashboard. With all your payment information in one place, you'll be a more"
                        title5="organized and efficient business owner."
                        />
                        <div className="text-center"> 
                            <Btn 
                                    btn="Start Now!"
                                    />
                        </div>
                    
            </div>

        </div>
            <div className="container-fluid">
                    <div className="container">
                       <Dashboard 
                       imgsrc={photo}
                       />
                    </div>
            </div>
            <div className="container-fluid bg-sec">
                    <div className="container">
                      
            <Smart 
            title1="Smart Payment"
            title2="Remove payment pains with"
            title3="Direct Debit"
            title4="This should be used to tell a story about your product"
            title5="or service. How"
            title6="or service. How"
            imgsrc={photo1}
            
            />
          
            </div>
            </div>
</>
    );
}
export default Features;

