
import React from 'react';
import '../fan/smart.css';
import LocalShippingIcon from '@material-ui/icons/LocalShipping';


const Smart = (props) => {
return(
<>
        <div className="row">
            
            <div className="col-sm-6">
            <div className="cta-content-five">
<span>{props.title1}</span>
                <h2>{props.title2} <br />{props.title3}</h2>
                <p>{props.title4} 
                {props.title5}<br />
                {props.title6}</p>
                     
        </div>
       
            <div className="row">
        <div className="col-sm-4">

        <div className="cardcar-sec">
                <div className="text-center"><LocalShippingIcon /></div>
                <h3 className="text-center">One Fee</h3>
                </div>
        </div>
        <div className="col-sm-4">
            
        <div className="cardcar-sec">
                <div className="text-center"><LocalShippingIcon /></div>
                <h3 className="text-center">One Fee</h3>
                </div>
        </div>
        <div className="col-sm-4">
            
        <div className="cardcar-sec">
                <div className="text-center"><LocalShippingIcon /></div>
                <h3 className="text-center">One Fee</h3>
                </div>
        </div>
            </div>
        </div>
        <div className="col-sm-6">
           <img src={props.imgsrc} alt="" className="sliderimg"/>
        </div>
        </div>
</>
);
}

export default Smart;


