import React from 'react';
import '../fan/features.css';
import SearchIcon from '@material-ui/icons/Search';


const Card = (props) => {
    return(
<>

                <div className="col-md-4 col-sm-12 mx-auto">
               
                                    <div className="cards">
                                       
                    <div className="text-center"> <SearchIcon /></div>
                    <div className="card-bodys">
    <h5 className="card-titles text-center">{props.title}</h5>
                    </div>
                    </div>
                </div>
             

</>
    );
}
export default Card;

