import React from 'react';
import '../main-Header/btn.css';
import { NavLink } from 'react-router-dom';

const Btn = (props) => {

    return(
        <>
        
<li className="nav-item">
                   
    <NavLink className="btn btn-outline my-2 my-sm-0" to="/sing" >{props.btn}</NavLink>
               
                 </li>
            
        </>
    )
}

export default Btn;