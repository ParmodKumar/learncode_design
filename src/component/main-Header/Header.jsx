import React from 'react'

import '../main-Header/header.css';
import logo from '../main-Header/imges/logo.png';
import Btn from '../main-Header/Btn';
import Brands from '../API/Brands';
import { Link, animateScroll as scroll } from "react-scroll";

const Header = (props) => {
    return(
    <>
    
      <div className="container header-sec">
        <nav className="navbar navbar-expand-lg navbar-light">
          <img src={logo} alt="logo" />                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                  <Link
                  activeClass="active"
                  to="/"
                  spy={true}
                  smooth={true}
                  className="nav-link"
                  offset={-75}
                  duration={500}
                  >{props.navbar1}</Link>
               
                  </li>
                  <li className="nav-item">
                  <Link
                  activeClass="active"
                  to="features"
                  spy={true}
                  smooth={true}
                  className="nav-link"
                  offset={10}
                  duration={500}
                  >{props.navbar2}</Link>
                
                  </li>
                
                  <li className="nav-item">
                  <Link
                  activeClass="active"
                  to="work"
                  spy={true}
                  smooth={true}
                  className="nav-link"
                  offset={0}
                  duration={500}
                  >{props.navbar3}</Link>
                 
                  </li>
                  <li className="nav-item">
                  <Link
                  activeClass="active"
                  to="support"
                  spy={true}
                  smooth={true}
                  className="nav-link"
                  offset={-10}
                  duration={500}
                  >{props.navbar4}</Link>
                  
                  </li>
                  <Link className="btn my-2 my-sm-0" to="/brands" >Brands</Link>
                  <Btn 
                  btn="Sing in"
                  
                  />
                 
                </ul>
           
              </div>
        </nav>
      </div>    
     
    </>
    );
}
export default Header;
