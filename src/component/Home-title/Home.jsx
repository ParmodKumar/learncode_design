import React from 'react';
import Typography from '@material-ui/core/Typography';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import logo from "../Home-title/Homeimg/logo.jpg";
import '../Home-title/home.css';
import Slider from '../Home-title/Slider';
import Features from "../fan/Features";
import logo1 from '../Home-title/Homeimg/logo1.png';
import logo2 from '../Home-title/Homeimg/logo2.png';
import logo3 from '../Home-title/Homeimg/logo3.png';
import logo4 from '../Home-title/Homeimg/logo4.png';
import logo5 from '../Home-title/Homeimg/logo5.png';
import logo6 from '../Home-title/Homeimg/logo6.png';
import Work from '../Works/Work';
import Support from '../Support-sec/Support';

    const Home = (props) => {
        return (
            <>
            <div className="container-fluid" id="/">
                <div className="row">
                    <div className="col-sm-6 background-color">
                        <div className="payment-processing offset-sm-1">
                            <Typography variant="h3" gutterBottom>
                                {props.title} 
                                {props.title1}
                            </Typography>
                            <Typography variant="body1" gutterBottom>
                                {props.para}
                                {props.para1}
                                {props.para2}
                            </Typography>
                                <li ><NavLink to="/">{props.link}</NavLink></li>
                                <li><NavLink to="/">{props.link1}</NavLink></li>
                                <li><NavLink to="/">{props.link2}</NavLink></li>
                            <Button variant="contained" type="button" className="btn btn-primary mt-4 mb-5">
                            {props.btn}
                            </Button>

                        </div>
                    </div>
                        <div className="col-sm-6 p-0">
                            <div className="payment-processing-banner ">
                                <img src={logo} alt="logo" />
                            </div>
                        </div>
                </div>
            </div>
                <Slider 
                    imgsrc1={logo1}
                    imgsrc2={logo2}
                    imgsrc3={logo3}
                    imgsrc4={logo4}
                    imgsrc5={logo5}
                    imgsrc6={logo6}
                    imgsrc7={logo2}
                    imgsrc8={logo5}
                /> 
                <Features  
            
                />
                <Work 
                />
               <Support
               />
            </>
);                                                                                                                                                                                                                                                              
}

export default Home;
