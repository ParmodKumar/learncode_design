import React from 'react';
import InfiniteCarousel from 'react-leaf-carousel';
import Typography from '@material-ui/core/Typography';
import '../Home-title/slider.css';



const Slider = (props) => {
  
  return(
          <>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-12">
                <Typography variant="h6" gutterBottom className="text-center pt-5 mb-4">
                  Trusted by over 2.5 milions company
                </Typography>   
                <InfiniteCarousel

                  breakpoints={[
                      {
                        breakpoint: 320,
                        settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                      },
                      },
                      {
                        breakpoint: 480,
                        settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                      },
                      },
                      {
                        breakpoint: 768,
                        settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                      },
                      },
                      {
                        breakpoint: 980,
                        settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        },
                        },
                      ]}
                        showSides={true}
                        sidesOpacity={0}
                        sideSize={.4}
                        slidesToScroll={1}
                        slidesToShow={6}
                        scrollOnDevices={true}
                      
                    >        
                    <div>
                      <img  alt='logo1'src={props.imgsrc1} className="img-sec" />
                    </div>
                    <div>
                      <img  alt='logo1'src={props.imgsrc2} className="img-sec" />
                    </div>
                    <div>
                      <img  alt='logo1'src={props.imgsrc3} className="img-sec" />
                    </div>
                    <div>
                      <img  alt='logo1'src={props.imgsrc4} className="img-sec" />
                    </div>
                    <div>
                      <img  alt='logo1'src={props.imgsrc5} className="img-sec" />
                    </div>
                    <div>
                      <img  alt='logo1'src={props.imgsrc6} className="img-sec" />
                    </div>
                    <div>
                      <img  alt='logo1'src={props.imgsrc2} className="img-sec" />
                    </div>
                    <div>
                      <img  alt='logo1'src={props.imgsrc5} className="img-sec" />
                    </div>
  </InfiniteCarousel>
  </div>
  </div>
  </div>
  </>
  
  );
}

export default Slider;
 