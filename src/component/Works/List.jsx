import React from 'react';
import { NavLink } from 'react-router-dom';
import FormatColorFillIcon from '@material-ui/icons/FormatColorFill';
import CropIcon from '@material-ui/icons/Crop';
import EditIcon from '@material-ui/icons/Edit';
import AttachMoneyIcon from '@material-ui/icons/AttachMoney';
import BuildIcon from '@material-ui/icons/Build';
import PieChartIcon from '@material-ui/icons/PieChart';

const List = (props) => {
return(
    <>
         <div className="col-sm-3">
            <div className="navbar-sec">
                <div className="nav-sec">
                  <li className="nav-item">
<NavLink className="nav-link nav" to="/"><FormatColorFillIcon className="icon"/>{props.list1}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/"><CropIcon className="icon"/> {props.list2}</NavLink>
                  </li>
                
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" ><EditIcon className="icon"/> {props.list3}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" ><AttachMoneyIcon className="icon"/> {props.list4}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" ><BuildIcon className="icon"/> {props.list5}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" ><PieChartIcon className="icon"/> {props.list6}</NavLink>
                  </li>
                  <li className="nav-item">
                    <NavLink className="nav-link nav" to="/" ><AttachMoneyIcon className="icon"/> {props.list7}</NavLink>
                  </li>
                 
          </div>
          </div>
          </div>
    </>
)
}

export default List;
