import React from 'react';
import Srevice from '../fan/Services';
import Datawork from '../Works/Datawork';
import Cardwork from '../Works/Cardwork';
import Link from '@material-ui/core/Link';
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';
import logo from '../Works/imges/logo.png';
import List from '../Works/List';
import Connect from '../Works/Connect';
import Dataconnect from '../Works/Dataconnect';
import dataCounter from '../Works/imges/dataCounter.jpg';
import Background from '../Works/Background';
import shop from '../Works/imges/shop.png';
import Shop from '../Works/Shop';
import Datashop from '../Works/Datashop';

const Work = () => {
return (
    <>
      <div className="container-fluid" id="work">
        <div className="container">
          <Srevice 
            title1="PROCESS"
            title2="How It Works"  
            class="services"
            title3="Quis ipsum suspendisse ultrices gravida. Risus commodo viverra"
            title4="maecenas accumsan lacus vel facilisis."                           
          />
        </div>  
      </div>
          <div className="container-fluid">
              <div className="container">
                  <div className="row">
                    <div className="col-sm-12 mx-auto">
                        <div className="row"> 
                            {
                            Datawork.map((val,id)=>{
                                return <Cardwork 
                                number={val.number}
                                custmear={val.custmear}
                                para={val.para}
                                para1={val.para1}
                                key={id}
                                />
                            })  
                            }
                        </div>
                    </div>
                  </div>
                    <div className="alert-info-box text-center">
                        <Link to="/">
                            Learn more about collecting Invoice payments<ArrowRightAltIcon/>
                        </Link>
                    </div>
              </div>
          </div>
                <div className="container-fluid bg">
                    <div className="container">
                      <div className="new-features-update">
                        <div className="bh">
                          <Srevice 
                              title1="Award-Winning Support"
                              title2="Meet our new updated Screen"
                              class="updated"
                              title3="Quis ipsum suspendisse ultrices gravida. Risus commodo viverra"
                              title4="maecenas accumsan lacus vel facilisis."
                          />
                        </div>
                      </div>
                    </div>
                    <div className="container">
                        <div className="row pt-5 mt-4">
                          <List
                            list1="one click demo import"
                            list2=" drop & drag editing"
                            list3 = "in-line editing" 
                            list4=" budget overview"
                            list5=" create & adjust"
                            list6=" view reports"
                            list7=" create & adjust"
                          />
                          <div className="col-sm-9">
                            <img src={logo} alt="logo" className="tab_content"/>
                          </div>
                        </div>
                    </div>
                </div>   

                <div className="container-fluid">
                  <div className="container">
                    <Srevice 
                      class="services"  
                          title1="CONNECT WITH US"
                          title2="Interact With Your Users On"
                          title="Every Single Platform"  
                    />


                  </div>
                  </div>  

                  <div className="container-fluid">
                    <div className="container">
                    <div className="row mb-5 pb-3">
                    <div className="col-sm-12 mx-auto">
                        <div className="row"> 
                        {
                            Dataconnect.map((val,id)=>{
                                return <Connect 
                                imgsrc={val.imgsrc}
                                boxtitle={val.boxtitle}
                                key={id}
                                />
                            })  
                            }
                            
                        </div>
                    </div>
                  </div>
                    </div>
                    </div>  
                    <div className="container-fluid bk p-0">
                    <section style={{backgroundImage: "url('" + dataCounter + "')"}} className="background-img">
                   
                    <Background 
                    cou="555"
                    k="k"
                    title="Trusted by"
                    title1="Businesses"
                    />
                     
                     <Background 
                    cou="555"
                    k="B"
                    dolor="$"
                    title="Processing"
                    title1="Businesses"
                    />
                     
                     <Background 
                    cou="555"
                    k="k"
                    title="Trusted by"
                    title1="Software Platform Integrations"
                    />
                     
                    
                    
                  </section>
                    </div>

                    <div className="container-fluid">
                      <div className="container">
                      <Srevice 
                        title1="SHOPPING"
                        title2="Shop 50 million online stores"
                        title="and counting."
                        class="services"
                        title3="Quis ipsum suspendisse ultrices gravida. Risus commodo viverra"
                        title4="maecenas accumsan lacus vel facilisis."
                       
                        />
                      
                      </div>
                    </div>
                    <div className="container-fluid">
                      <div className="container">
                        <div className="text-center">
                        <img src={shop} alt="shop" className="shopimg"/>
                        </div>
                      </div>
                      </div>
                      <div className="container-fluid">
              <div className="container">
                  <div className="row">
                    <div className="col-sm-12 mx-auto">
                        <div className="row"> 
                        {
                          Datashop.map((val,id)=>{
                            return <Shop 
                            key={id}
                            title={val.title}
                            icon={val.icon}
                            para1={val.para1}
                            para2={val.para2}
                            para3={val.para3}
                            />
                          })
                        }
                        </div>
                    </div>
                  </div>
                  </div>
                  </div>
  </>
)
}

export default Work;



 
