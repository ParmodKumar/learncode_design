import React from 'react';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import '../Works/shop.css';

const Shop = (props) => {
    return(
        <>
        <div className="col-md-4 col-sm-12 mx-auto">
               
               <div className="shop-sec">
                 
                   <div className="single-item">
    <div className="iconshop" >
 <ShoppingCartIcon className="icon"/>
    </div>
    
    <h4>{props.title}</h4>       
    <p>{props.para1}<br />{props.para2}<br />{props.para3}</p>
    </div>
</div>
</div>


        </>
    );
}

export default Shop;