import box from '../Works/imges/box.png';
import slack from '../Works/imges/slack.png';
import twitter from '../Works/imges/twitter.png';
import instagram from '../Works/imges/instagram.png';

const Dataconnect = [
    {
        imgsrc:box,
        boxtitle:"Mail Chimp",
    },
    {
        imgsrc:slack,
        boxtitle:"Slack",
    },
    {
        imgsrc:twitter,
        boxtitle:"Twitter",
    },
    {
        imgsrc:instagram,
        boxtitle:"Instagram",
    },
   
  
]

export default Dataconnect;
