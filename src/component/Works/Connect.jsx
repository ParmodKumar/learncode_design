import React from 'react';
import '../Works/box.css';

const Connect = (props) => {
    return(
        <>
        <div className="col-md-3 col-sm-12 mx-auto">
               
               <div className="box-sec">
                   <div className="platform-box">
                       <div className="box-img-sec d-flex">
   <img src={props.imgsrc} alt="img" className="box-img"/>       
    <h6>{props.boxtitle}</h6>
    </div>
    </div>
</div>
</div>
        </>
    );
}

export default Connect;